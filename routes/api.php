<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


Route::post('/api/users', 'UserController@store');
Route::put('/api/users/{transactionType}', 'UserController@update');
Route::get('/api/users', 'UserController@index');
Route::get('/api/users/{user}', 'UserController@show');
Route::delete('/api/users/{user}', 'UserController@destroy');

