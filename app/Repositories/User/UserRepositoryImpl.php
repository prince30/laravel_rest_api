<?php


namespace App\Repositories\User;

use App\Models\User;

class UserRepositoryImpl implements UserRepository
{
    /**
     * @param $data
     * @return mixed|void
     */
    public function store($data): User
    {
		$user = new User;

		$user->name = $data['name'];

		$user->save();
    }

    /**
     * @return mixed\|void
     */
    public function all()
    {
		return User::get(); 	
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function find($id)
    {
		return User::find($id);
    }

    /**
     * @param $data
     * @return mixed|void
     */
    public function update($id, $data)
    {
		$user = $this->find($id);
		
		$user->name = $data['name'];

		$user->save();
		
		return $user;
    }


    /**
     * @param $data
     * @return mixed|void
     */
    public function delete($id)
    {
		$user = $this->find($id);

		return $user->delete();
    }
}
