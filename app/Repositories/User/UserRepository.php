<?php


namespace App\Repositories\User;


interface UserRepository
{
    /**
     * @param $data
     * @return mixed
     */
    public function store($data);

    /**
     * @return mixed\
     */
    public function all();

    /**
     * @return mixed
     */
    public function find($id);

    /**
     * @param $data
     * @return mixed
     */
    public function update($id, $data);

    /**
     * @param $data
     * @return mixed
     */
    public function delete($id);
}
