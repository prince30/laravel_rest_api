<?php

namespace App\Response;

use Illuminate\Http\Response;

class ApiSuccessResponse
{
    private $statusCode;
    private $message;
    private $data;

    public function __construct($message, $data=null)
    {
        $this->data = $data;
        $this->message = $message;
        $this->statusCode = Response::HTTP_OK;
    }

    public function getPayload(){
        return [
            'success' => true,
			'status_code' => $this->getStatusCode(),
            'message' => $this->getMessage(),
            'data' =>  $this->getData()
        ];
    }

    public function getStatusCode()
    {
        return $this->statusCode;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getData()
    {
        return $this->data;
    }
}
