<?php

namespace App\Http\Controllers;

use App\Exceptions\ApiDataNotFoundException;
use App\Exceptions\ApiInternalServerError;
use App\Response\ApiSuccessResponse;
use App\Http\Resources\API\V1\User as UserResource;
use App\Repositories\User\UserRepository;
use Illuminate\Http\Request;
use Illuminate\Validation\ValidationException;

class UserApiController extends Controller
{
    /**
     * @var
     */
    private $userRepository;

    /**
     * UserApiController constructor.
     * @param UserRepository $userRepository
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    /**
     *
     * @throws ApiDataNotFoundException
     */
    public function index(): array
    {
        $users = $this->userRepository->all();

        if (!$users) throw new ApiDataNotFoundException();

        return (new ApiSuccessResponse('Users', UserResource::collection($users)))->getPayload();
    }

    /**
     * @param Request $request
     * @return ResponsePayload
     * @throws ApiDataNotFoundException|ValidationException
     */
    public function store(Request $request): array
    {
        $this->validate($request, [
            'name' => 'required'
        ]);

        $data = $request->all();

        $user = $this->userRepository->store($data);

        if (!$user) throw new ApiInternalServerError();

        return (new ApiSuccessResponse('User has created', new UserResource($user)))->getPayload();
    }

    /**
     * @param $data
     * @return ResponsePayload
     */
    public function show($id): array
    {
        $user = $this->userRepository->find($id);

        if (!$user) throw new ApiDataNotFoundException();
		
        return (new ApiSuccessResponse('User', new UserResource($user)))->getPayload();
    }

    /**
     * @param Request $request
     * @param $data
     * @return ResponsePayload
     * @throws ValidationException
     * @throws ApiDataNotFoundException
     */
    public function update(Request $request, $id): array
    {
        $this->validate($request, [
            'name' => 'required',
        ]);

        $data = $request->all();

        $user = $this->userRepository->update($id, $data);
		
        if (!$user) throw new ApiInternalServerError();

        return (new ApiSuccessResponse('User has updated', new UserResource($user)))->getPayload();
    }

    public function destroy($id): array
    {
        $this->userRepository->delete($id);

        return (new ApiSuccessResponse('User has deleted', null))->getPayload();
    }
}

