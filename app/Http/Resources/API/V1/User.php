<?php

namespace App\Http\Resources\API\V1;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     * @param $request
     * @return array
     */

    public function toArray($request): array
    {
        return [
            'name' => $this->name
        ];
    }
}

