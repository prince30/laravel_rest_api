<?php

namespace App\Exceptions;


use Exception;
use Illuminate\Http\Response;
use Throwable;

class ApiException extends \Exception
{
    private $message;
    private $statusCode;
    private $developerMessage;

    public function __construct($message = "Internal Server Error", $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR, $developerMessage="")
    {
        parent::__construct($message, $statusCode, null);
        $this->message = $message;
        $this->statusCode = $statusCode;
        $this->developerMessage = $developerMessage;
    }

    public function getPayload(){
        return [
            'success' => false,
            'status_code' => $this->getStatusCode(),
            'message' => $this->getErrorMessage(),
            'developer_message' => $this->getDeveloperMessage(),
        ];
    }

    public function getStatusCode(){
        $this->statusCode;
    }

    public function getErrorMessage(){
        $this->message;
    }

    public function getDeveloperMessage(){
        $this->developerMessage;
    }
}
