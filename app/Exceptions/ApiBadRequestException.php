<?php


namespace App\Exceptions;

use Illuminate\Http\Response;

class ApiBadRequestException extends ApiException
{
   public function __construct($message = "Bad Request", $developerMessage = "")
   {
       parent::__construct($message, Response::HTTP_BAD_REQUEST, $developerMessage);
   }
}
