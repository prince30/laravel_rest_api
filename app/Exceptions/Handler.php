<?php

namespace App\Exceptions;

use App\Responses\ErrorResponsePayload;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that should not be reported.
     *
     * @var array
     */
    protected $dontReport = [
        AuthorizationException::class,
        HttpException::class,
        ModelNotFoundException::class,
        ValidationException::class,
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param \Throwable $exception
     * @return void
     *
     * @throws \Exception
     */
    public function report(Throwable $exception)
    {
        parent::report($exception);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param Request $request
     * @param \Throwable $e
     * @return JsonResponse
     *
     * @throws \Throwable
     */
    public function render($request, Throwable $e): JsonResponse
    {
        $request->headers->set('accept', 'application/json');

        $apiException = $this->getApiException($e);

        return response()->json($apiException->getPayload(), $apiException->getStatusCode());
    }

    private function getApiException(Throwable $e)
    {
        if ($e instanceof ApiException) {
            return $e;
        } elseif ($e instanceof HttpResponseException) {
            return new ApiException($e->getResponse(), $e->getCode());
        } elseif ($e instanceof ModelNotFoundException) {
            $e = new NotFoundHttpException($e->getMessage(), $e);
            return new ApiException($e->getMessage(), $e->getCode());
        } elseif ($e instanceof AuthorizationException) {
            $e = new HttpException(403, $e->getMessage());
            return new ApiException($e->getMessage(), $e->getStatusCode());
        } elseif ($e instanceof ValidationException && $e->getResponse()) {
            return new ApiException($e->getMessage(), $e->status, $e->getResponse()->original);
        }
        return new ApiInternalServerError($e->getMessage());
    }
}