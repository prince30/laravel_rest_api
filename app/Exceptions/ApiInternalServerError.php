<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
class ApiInternalServerError extends ApiException
{
    public function __construct($developerMessage = "")
    {
        $message = "Internal Server Error";
        $statusCode = Response::HTTP_INTERNAL_SERVER_ERROR;
        parent::__construct($message, $statusCode, $developerMessage);
    }
}
