<?php

namespace App\Exceptions;

use Illuminate\Http\Response;
class ApiDataNotFoundException extends ApiException
{
    public function __construct($message = "No record found")
    {
        parent::__construct($message, Response::HTTP_NOT_FOUND, $developerMessage = "");
    }
}
